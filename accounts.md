# User Accounts

Here is a list of all Accounts used in this project.

## Ubuntu Server

This server hosts Jenkins and the docker with all container.

Type | Value
--- | ---
U | nico
P| password


## Jenkins

Is for continuous integration of the microservice

Type | Value
--- | ---
U | nico
P| 83d71644a9234829a7e4fa70e6a0f82b

## Docker Portainer

Hosts all microservices, the database and the API.

Type | Value
--- | ---
U | admin
P| lJ2R0XYXs1NV4QiQ4eOj

## SQL Server

This database holds all the information needed for the project.

Type | Value
--- | ---
U | SA
P| \<CacXTecr\>

## Gitlab

Type | Value
--- | ---
Root U | root
Root P| 83d71644a9234829a7e4fa70e6a0f82b
U | Vorname
P | VornameGeburtsjahr (Wechsel bei erstem Login)

## Redmine

Type | Value
--- | ---
Root U | admin
Root P| 83d71644a9234829a7e4fa70e6a0f82b
U | Vorname
P | VornameGeburtsjahr

## Achiva

Type | Value
--- | ---
U | admin
P| Mgmu9*#XgOI!
