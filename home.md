# Web Engineering Project
This is a documentation of the project lifecykle.

## [Setup](https://gitlab.com/web211/docu/wikis/setup)

## [Accounts](https://gitlab.com/web211/docu/wikis/accounts)

## [Docker](https://gitlab.com/web211/docu/wikis/docker)

## [REST](https://gitlab.com/web211/docu/wikis/rest)

>The git repos use this [gitignore](https://www.gitignore.io/api/java,maven,eclipse,intellij)


