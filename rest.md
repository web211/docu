
# Table of contents

- [Overview](#overview)
- [REST Definition](#rest-definition)
- [Login](#login)
- [Logout](#logout)
- User
    - [Create User](#create-user)
    - [Read User](#read-user)
    - [Update User](#update-user)
    - [Delete User](#delete-user)

# Overview

*Does not include url parameter*

Every url starts with: */api*

URL | HTTP | Detail
--- | --- | ---
/login | GET | Login with user
/logout | GET | logout
/user | GET | read all user
/user | POST | create user
/user/{id} | GET | read user
/user/name/{username} | GET | read user by username
/user/{id} | PUT | update user
/user/{id} | DELETE | delete user
/user/{id}/comment | GET | read all comments of an user
/user/{id}/comment | POST | create comment to an user
/user/{id}/follow | GET | read all or one follower of an user
/user/{id}/follow | PUT | toggle follow of an user to another user
/question | GET | read all questions
/question | POST | create question
/question/{id} | GET | read one question
/question/{id} | PUT | update question
/question/{id} | DELETE | delete question
/question/{id}/answer | GET | read all answers of question
/question/{id}/answer | POST | create answer to question
/question/{id}/comment | GET | read all comments of question
/question/{id}/comment | POST | create comment to question
/question/{id}/follow | GET | read all follower of question
/question/{id}/follow | POST | toggle follow of an user to question
/question/{id}/like | GET | read all likes of question
/question/{id}/like | POST | toggle like of an user to question
/answer/{id} | GET | read answer
/answer/{id} | PUT | update answer
/answer/{id} | DELETE | delete answer
/answer/{id}/comment | GET | read all comments of answer
/answer/{id}/comment | POST | create comment to answer
/answer/{id}/like | GET | read all likes of answer
/answer/{id}/like | POST | toggle like of an user to answer
/comment/{id} | GET | read comment
/comment/{id} | PUT | update comment
/comment/{id} | DELETE | delete comment
/comment/{id}/like | GET | read all likes of comment
/comment/{id}/like | POST | toggle like of an user to comment
/search?text={}&type={} | GET | search an item
/search/content | GET | search for all items which the user created
/search/follow | GET | get a list of all followed contents
/search/like | GET | get a list of all liked content
/follow | GET | get all follows
/follow/{type}/{id} | GET | read follows of a type from one user or all of them
/like | GET | get all likes
/like/{type}/{id} | GET | read likes of a type from one user or all of them
/notify/{id} | GET | read all notifications of a user
/notify/{id}/latest | GET | read latest notifications of an user

# REST Definition

In this chapter the REST API is explained.

Action | Method
--- | ---
Create | **PUT** with a new URI
 | **POST** to a base URI returning a newly created URI
Read | **GET**
Update | **PUT** with an existing URI
Delete | **DELETE**

> More information about [HTTP Codes](https://de.wikipedia.org/wiki/HTTP-Statuscode)

## Create User

This is used to create a new User

#### URL - POST

````
/user
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
username | required | username
email | required | email
password | required | md5 hash of the password
firstname | optional | first name
lastname | optional | last name

#### Result

Returns the newly created user

---
---

## Read all Users 

This is used to read all users

#### URL - GET

````
/user
````

#### Parameters

*None*

#### Result

list of users

---
---

## Read User

Get information of one user

#### URL - GET

````
/user/{id}
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | user id

#### Result

user

---
---

## Read User by Username

Get information of one user by its username

#### URL - GET

````
/user/name/{username}
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
username | required | usernmae

#### Result

user

---
---
   

## Update User

Update user information. only the given information will be updated.

#### URL - PUT

````
/user/{id}
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | user id
email | optional | email
password | optional | md5 hash of the password
firstname | optional | first name
lastname | optional | last name

---
---

## Delete User

delete a user

#### URL - DELETE

````
/user/{id}
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | user id

---
---

## Read users which follow user

read users which follow another user or read only one user which follows this user.

#### URL - GET

````
/user/{id}/follow
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | user id
userId | optional | id of user which follows the user

#### Result

list of all follower.
does contain only one element if the userId is given

---
---

## toggle user follow

toggle the follow state for a user on another user.

- userId is the user which will follow another user
- id is the user which will be followed

#### URL - PUT

````
/user/{id}/follow
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | user id
userId | required | follower id

#### Result

follow

---
---

## add comment to user

add a comment which will be shown on the profile page of an user

#### URL - POST

````
/user/{id}/comment
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | id of user where the comment whill be shown on the profile page
text | required | text of the comment
userId | required | user which creats the comment


#### Result

comment

---
---

## read comments for a user

read all comments for an user. these are intended to be on the profile page

#### URL - GET

````
/user/{id}/comment
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | if from user were the comments were added


#### Result

list of comments

---
---

## read all question

read all question

#### URL - GET

````
/question
````

#### Parameters

*None*

#### Result

list of question

---
---

## create question

create a new question

#### URL - POST

````
/question
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
title | required | question title
text | required | question text
userId | required | user which creates the question

#### Result

the newly created question

---
---

## read question by id

read one question by id

#### URL - GET

````
/question/{id}
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id

#### Result

one question

---
---

## update question

update a question. does only change the given information

#### URL - PUT

````
/question/{id}
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id
title | optional | question title
text | optional | question text
userId | required | user which updated the question

#### Result

*None*

---
---

## delete question

delete question

#### URL - DELETE

````
/question/{id}
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id

#### Result

*none*

---
---

## add answer to question

add an answer to a question

#### URL - PUT

````
/question/{id}/answer
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id where the answer will be added
text | required | answer text
userId | required | user id how creates the answer


#### Result

answer

---
---

## read all answers of a question

read all answers of a question

#### URL - GET

````
/question/{id}/answer
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id where you want to get the answers


#### Result

list of answers

---
---

## add comment to question

add a comment to a question

#### URL - PUT

````
/question/{id}/comment
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id
text | required | comment text
userId | required | user id how wants to add a comment 


#### Result

the newly created comment

---
---

## read all comments from a question

read all comments from a question

#### URL - GET

````
/question/{id}/comment
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id

#### Result

list of comments

---
---

## toggle question follow

toggle follow from a user to a question

#### URL - POST

````
/question/{id}/follow
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id 
userId | required | user id how wants to follow the question


#### Result

follow

---
---

## read all follower of question

read all follower of a question

#### URL - GET

````
/question/{id}/follow
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id 


#### Result

list of follow

---
---

## read all likes of a question

read all likes of a question

#### URL - GET

````
/question/{id}/like
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id


#### Result

list of likes

---
---

## toggle question like

toggle a like for a question from an user

#### URL - POST

````
/question/{id}/like
````

#### Parameters

Name | opt/req | Details
--- | --- | ---
id | required | question id
userId | required | user id how wants to like the question


#### Result

like

---
---