## Steps
**1. Setup Ubuntu 18.04 server**

Install the server in a VM or use a v-server. The first step is to update the server with:
````shell
sudo apt-get update
sudo apt-get upgrade
````
> in case that the keyboard layout is wrong, use the following command
> ````shell
> sudo dpkg-reconfigure keyboard-configuration
> ````

**2. Setup docker**

Install docker which will be used to host all java processes, the database, etc.
This is explained here: [How to Install and Use Docker on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)

**3. download Portainer**

The easyest way to manage docker is with portainer.
This is explained here: [Install and Configure Portainer](https://www.howtoforge.com/tutorial/ubuntu-docker-portainer/#step-install-and-configure-portainer)

**4. Create a docker network**

This will be used for all the docker images to communicate with each other.

>TODO

**5. install these Container**

- Gitlab
- SQL Server
- Artifactory
